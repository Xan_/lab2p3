from django.urls import include, path
from rest_framework import routers
from .views import DriversViewSet, VehicleViewSet, VehicleBrandViewSet, VehicleModelViewSet, VehicleImageViewSet, \
    VehicleTypeViewSet

router = routers.DefaultRouter()
router.register(r'drivers', DriversViewSet)
router.register(r'vehicles', VehicleViewSet)
router.register(r'vehiclesBrand', VehicleBrandViewSet)
router.register(r'vehiclesModel', VehicleModelViewSet)
router.register(r'vehiclesImage', VehicleImageViewSet)
router.register(r'vehiclesType', VehicleTypeViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
