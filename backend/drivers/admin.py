from django.contrib import admin
from drivers.models import *


# Register your models here.
admin.site.register(Driver)
admin.site.register(VehicleModel)
admin.site.register(Vehicle)
admin.site.register(VehicleType)
admin.site.register(VehicleBrand)
admin.site.register(VehicleImage)
