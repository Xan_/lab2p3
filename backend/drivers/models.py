from django.db import models
import core.models
from enum import Enum


class Driver(models.Model):
    bio = models.CharField(max_length=1024, blank=True)
    user = models.ForeignKey(core.models.User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.user}"


class VehicleBrand(models.Model):
    name = models.CharField(max_length=254)
    description = models.CharField(max_length=245, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.name}"


class VehicleType(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=245, blank=True)
    image = models.ForeignKey(core.models.Image, blank=True, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.name}"


class VehicleModel(models.Model):
    name = models.CharField(max_length=254)
    volume = models.DecimalField(decimal_places=5, max_digits=20)
    payload = models.DecimalField(decimal_places=5, max_digits=20)
    type = models.ForeignKey(VehicleType, on_delete=models.DO_NOTHING)
    description = models.CharField(max_length=254, blank=True)
    brand = models.ForeignKey(VehicleBrand, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.name}"


class VehicleStatus(Enum):
    active = 0
    inactive = 1
    broken = 2

    @classmethod
    def values(cls):
        return [(attr.name, attr.value) for attr in cls]


class Vehicle(models.Model):
    registration_plate = models.CharField(max_length=50)
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    model = models.ForeignKey(VehicleModel, on_delete=models.DO_NOTHING)
    status = models.IntegerField(choices=VehicleStatus.values())
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.model} - {self.registration_plate}"


class VehicleImage(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    image = models.ForeignKey(core.models.Image, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')
