from django.contrib.auth.models import AbstractUser
from django.db import models
from enum import Enum
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
from core.user_manager import UserManager


class UserRole(Enum):
    admin = 0
    user = 1
    driver = 2

    @classmethod
    def values(cls):
        return [(attr.value, attr.name) for attr in cls]


class City(models.Model):
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.name}"


class Address(models.Model):
    full_address = models.CharField(max_length=255, blank=True)
    location = models.CharField(max_length=255)
    house = models.IntegerField()
    flat = models.IntegerField(blank=True)
    street = models.CharField(max_length=255)
    district = models.CharField(max_length=255, blank=True)
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.full_address}"


class Image(models.Model):
    title = models.CharField(max_length=255, blank=True)
    url = models.FileField()
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.title}"


class User(AbstractUser):
    first_name = models.CharField(max_length=224)
    second_name = models.CharField(max_length=255)
    third_name = models.CharField(max_length=255, blank=True)
    email = models.EmailField(unique=True, blank=False)
    phone_number = PhoneNumberField()
    role = models.IntegerField(choices=UserRole.values())
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    avatar = models.ForeignKey(Image, on_delete=models.CASCADE)
    role = models.IntegerField(choices=UserRole.values(), default=UserRole.user.value)
    address = models.ForeignKey(Address, null=True, on_delete=models.CASCADE)
    avatar = models.ForeignKey(Image, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.first_name} {self.second_name} {self.third_name}"

    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()


class Message(models.Model):
    title = models.CharField(max_length=255, blank=True)
    text = models.CharField(max_length=255)
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='authored_messages')
    to = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.title}, {self.text}"
