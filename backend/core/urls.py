from django.urls import include, path
from rest_framework import routers
from .views import UserViewSet, AddressViewSet, CityViewSet, ImageViewSet, MessageViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'address', AddressViewSet)
router.register(r'city', CityViewSet)
router.register(r'image', ImageViewSet)
router.register(r'message', MessageViewSet)

urlpatterns = [
    path('', include(router.urls))
]
