from rest_framework import viewsets

from .models import Job, JobImage, Category, CategoryVehicleType, ReviewImage, Order, OrderStatus, \
    OrderCancellation, JobStatus, Offer, Review, UserReason, DriverReason

from jobs.serializers import JobSerializer, JobImageSerializer, CategorySerializer, CategoryVehicleTypeSerializer, \
    ReviewImageSerializer, OrderSerializer, OrderStatusSerializer, OrderCancellationSerializer, JobStatusSerializer, \
    OfferSerializer, ReviewSerializer, UserReasonSerializer, DriverReasonSerializer


class JobViewSet(viewsets.ModelViewSet):
    queryset = Job.objects.all().order_by('-created_at')
    serializer_class = JobSerializer


class JobImageViewSet(viewsets.ModelViewSet):
    queryset = JobImage.objects.all().order_by('-created_at')
    serializer_class = JobImageSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by('-created_at')
    serializer_class = CategorySerializer


class CategoryVehicleTypeViewSet(viewsets.ModelViewSet):
    queryset = CategoryVehicleType.objects.all().order_by('-created_at')
    serializer_class = CategoryVehicleTypeSerializer


class ReviewImageViewSet(viewsets.ModelViewSet):
    queryset = ReviewImage.objects.all().order_by('-created_at')
    serializer_class = ReviewImageSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all().order_by('-created_at')
    serializer_class = OrderSerializer


class OrderCancellationViewSet(viewsets.ModelViewSet):
    queryset = OrderCancellation.objects.all().order_by('-created_at')
    serializer_class = OrderCancellationSerializer


class OfferViewSet(viewsets.ModelViewSet):
    queryset = Offer.objects.all().order_by('-created_at')
    serializer_class = OfferSerializer


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all().order_by('-created_at')
    serializer_class = ReviewSerializer


class ViewSet(viewsets.ModelViewSet):
    queryset = Job.objects.all().order_by('-created_at')
    serializer_class = JobSerializer

