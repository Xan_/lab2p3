from django.urls import include, path
from rest_framework import routers
from .views import JobViewSet, JobImageViewSet, CategoryViewSet, CategoryVehicleTypeViewSet, ReviewImageViewSet,\
    OrderViewSet, OrderStatusViewSet, OrderCancellationViewSet, JobStatusViewSet, OfferViewSet, ReviewViewSet, \
    UserReasonViewSet, DriverReasonViewSet


router = routers.DefaultRouter()
router.register(r'jobs', JobViewSet)
router.register(r'jobImages', JobImageViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'categoryVehicleTypes', CategoryVehicleTypeViewSet)
router.register(r'reviewImages', ReviewImageViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'orderStatuses', OrderStatusViewSet)
router.register(r'orderCancellations', OrderCancellationViewSet)
router.register(r'kobStatuses', JobStatusViewSet)
router.register(r'offers', OfferViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'userReasons', UserReasonViewSet)
router.register(r'driverReasons', DriverReasonViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
