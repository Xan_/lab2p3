import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
  IonModal,
} from '@ionic/react';
import React, {useEffect, useState} from 'react';
import './PlaceOrder.css';

import NewOrder, {MapAddress} from "../../components/NewOrder";
import { RouteComponentProps, useHistory } from "react-router";
import { YMaps, Map, Placemark, withYMaps} from "react-yandex-maps";

const OurMap = withYMaps((props: any) => {

  const [addresses, setAddresses] = useState([] as MapAddress[]);

  function onMapClick(event: any) {
    const coords = event.get('coords') as number[];

    if (addresses.length === 2) {
      setAddresses([{ coords }]);
    } else {
      setAddresses([...addresses, { coords }]);
    }
  }

  useEffect(() => {
    if (addresses.length > 0) {
      const coords = addresses[addresses.length - 1].coords;
      props.ymaps.geocode(coords).then(function (res: any) {
        const name = res.geoObjects.get(0).properties.get(0).name;
        const city = (res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName'));
        const newAddresses = [...addresses];
        newAddresses[addresses.length - 1].name = name;
        newAddresses[addresses.length - 1].city = city;
        console.log(newAddresses[addresses.length - 1].city)
        props.onAddressChange(newAddresses);
      });
    }
  }, [addresses]);

  return <Map
      width="100%" height="100%" defaultState={{ center: [43.3219485, 45.674585], zoom: 16 }}
      onClick={onMapClick}>
      {addresses.map(({coords}) => (
        <Placemark
          key={coords.join(",")}
          geometry={coords}
        />
      ))}
    </Map>
}, true, ['geocode']);

interface PlaceOrderPageProperties extends RouteComponentProps<{
  categoryId: string;
  history:any
}> { }

const PlaceOrderPage: React.FC<PlaceOrderPageProperties> = ({match}: PlaceOrderPageProperties) => {

  const history = useHistory();

  const [addresses, setAddresses] = useState({} as MapAddress[]);

  return (

    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Заказы</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <div className="z">
          <IonButton color="light" className="back-button" onClick={()=> {
                history.goBack()
              }}
           >&lt;</IonButton>
        </div>

        <div className="map-wrapper">
          <YMaps query={{ apikey: 'acd6a791-505b-40df-b03a-dd620566a93f' }} >
            <OurMap onAddressChange={(addresses: any) => {
              setAddresses(addresses);
            }}/>
          </YMaps>
        </div>
              
        <IonModal isOpen={true}>
          <NewOrder addresses={addresses} idCategory={match.params.categoryId} />
        </IonModal>
      </IonContent>
    </IonPage>
  );
};

export default PlaceOrderPage;
